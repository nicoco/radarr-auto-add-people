#!/usr/bin/env python3

import sys
import logging
from pprint import pprint

import requests
from slugify import slugify
import tmdbsimple as tmdb
import yaml

class Radarr:
    def __init__(self, host, port, apikey, user, password):
        self.user = user
        self.password = password
        self.apikey = apikey
        self.host = host
        self.port = port
        self.refresh_movies()
    
    def _get_request_params(self, endpoint):
        p = {"url": f"{self.host}:{self.port}/api/{endpoint}?apikey={self.apikey}"}
        if self.user is not None:
            p["auth"] = (self.user, self.password)
        return p
    
    def _request(self, method, endpoint, json=None, params=None):
        return getattr(requests, method)(**self._get_request_params(endpoint), json=json, params=params)

    def get_images(self, tmdb_id):
        r = self._request("get", f"movie/lookup/tmdb", params={"tmdbId": tmdb_id})
        json = r.json()
        images = json['images']
        return images

    def refresh_movies(self):
        radarr_movies = self._request("get", "movie").json()
        self.movies = [m['tmdbId'] for m in radarr_movies]

    def add_movies(self, movies):
        for movie in movies:
            if movie['tmdbId'] in self.movies:
                print(f"Skipping {movie['title']} ({movie['tmdbId']}) because it was already in Radarr")
                continue
            print(f"Adding {movie['title']} ({movie['tmdbId']})")
            r = self._request("post", "movie", json=movie)
            if r.status_code != 201:
                print(f"Something went wrong: Code {r.status_code}")
                pprint(r.json())


def tmdb_to_radarr(m):
    return {'title': m['original_title'],
            'qualityProfileId': config['radarr']['profiles'].get(
                                    m['original_language'],
                                    config['radarr']['profiles']['default']),
            'titleSlug': slugify(f"{m['original_title']}-{m['id']}"),
            'tmdbId': m['id'],
            'year': m['release_date'][:4],
            'images': radarr.get_images(m['id']),
            'rootFolderPath': config['radarr']['rootFolderPath'],
            'monitored': config['radarr']['monitored'],
            'addOptions': {"searchForMovie": config['radarr']['search']}}


def get_people_movies(people_id, roles):
    print(f"Getting movies for #{people_id}")
    people = tmdb.People(people_id)
    print(f"Getting movies for {people.info()['name']}")
    movie_credits = people.movie_credits()
    movies = []
    if 'actor' in roles:
        movies += [tmdb_to_radarr(m)
                   for m in movie_credits['cast']
                   if m['release_date']]
    for role in roles:
        movies += [tmdb_to_radarr(m)
                   for m in movie_credits['crew']
                   if role.lower() in m.get('job', '').lower()
                   if m['release_date']]
    return movies


if __name__ == "__main__":
    yaml_file = sys.argv[1]
    with open(yaml_file, 'r') as file:
        config = yaml.load(file.read())
    tmdb.API_KEY = config["tmdb_key"]
    radarr = Radarr(config['radarr']['host'],
                    config['radarr']['port'],
                    config['radarr']['key'],
                    config['radarr'].get('user'),
                    config['radarr'].get('pass'))

    movies = []
    for people_id, roles in config['people'].items():
        movies += [m for m in get_people_movies(people_id, roles)
                   if m['tmdbId'] not in [m['tmdbId'] for m in movies]]
    radarr.add_movies(movies)
