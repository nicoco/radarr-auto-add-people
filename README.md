# Radarr Auto Add People

## What is this?

A very simple python script to *follow* people with the automated movie downloader [Radarr](http://radarr.video/).

## How to install it?

Clone the repo and install its dependencies:

### With `pip`

If you are running this in virtualenv or something similar:

```
pip install requests slugify tmdbsimple yaml
```

### With `apt`

If you are in the debian master race like me, you could just:

```
apt install python3-requests python3-slugify python3-tmdbsimple python3-yaml
```

## How to use it?

Just modify the (hopefully) self-explanatory people.yaml.example file.
You will to have manually look for person ids in [TMDB](https://www.themoviedb.org/person/) to fill it.

Then launch the script with the yaml file as argument:

```
/path/to/radarr-auto-add-people/radarr_auto_add_people.py yourpeople.yaml
```

And automate this via cron or whatever you like.

## Bonus!

If like me you don't like dubbed movies, you can specify specific radarr profiles depending on the movie original language. Not as automated as I would like it, but meh, better than nothing.
